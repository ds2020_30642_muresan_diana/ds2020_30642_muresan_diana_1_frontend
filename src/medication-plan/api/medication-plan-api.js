import {HOST} from '../../commons/hosts';
import RestApiClient from "../../commons/api/rest-client";

const endpoint = {
    medicationPlan: '/medication-plan'
};

function getMedicationPlans(callback) {
    let request = new Request(HOST.backend_api + endpoint.medicationPlan, {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}



function postMedicationPlan(user, patientId, callback) {
    let request = new Request(HOST.backend_api + endpoint.medicationPlan + "/insert/" + patientId , {
        method: 'POST',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    });
    console.log("URL: " + request.url);
    RestApiClient.performRequest(request, callback);
}

function updateMedicationPlan(user, idMedicationPlan, idPatient, callback) {
    let request = new Request(HOST.backend_api + endpoint.medicationPlan + "/update/" + idMedicationPlan + "/" + idPatient, {
        method: 'PUT',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    });
    console.log("URL: " + request.url);
    RestApiClient.performRequest(request, callback);
}

export {
    getMedicationPlans,
    postMedicationPlan,
    updateMedicationPlan

};