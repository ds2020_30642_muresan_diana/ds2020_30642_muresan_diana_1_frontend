import React from 'react';
import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import {
    Button,
    Card,
    CardHeader,
    Col,
    Modal,
    ModalBody,
    ModalHeader,
    Row
} from 'reactstrap';

import * as API_USERS from "./api/medication-api"
import MedicationTable from "./components/medication-table";
import MedicationForm from "./components/medication-form";
import moment from "moment";
import PatientForm from "../patient/components/patient-form";
import PatientUpdateForm from "../patient/components/patient-update-form";
import MedicationUpdateForm from "./components/medication-update-form";
import {Redirect} from "react-router-dom";



class MedicationContainer extends React.Component {

    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.reload = this.reload.bind(this);
        this.reload1 = this.reload1.bind(this);
        this.medication = {
            id: null,
            name: null,
            sideEffects: null,
            dosage: null
        }
        this.state = {
            updateForm: false,
            selected: false,
            collapseForm: false,
            tableData: [],
            isLoaded: false,
            errorStatus: 0,
            error: null,
            id: null
        };
    }

    componentDidMount() {
        this.fetchMedications();
    }

    fetchMedications() {
        return API_USERS.getMedications((result, status, err) => {

            if (result !== null && status === 200) {
                this.setState({
                    tableData: result,
                    isLoaded: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    getMedication(id) {
        return API_USERS.getMedicationById(id, (result, status, err) => {
            if (result !== null && status === 200) {
                this.medication.id = result.id;
                this.medication.name = result.name;
                this.medication.sideEffects = result.sideEffects;
                this.medication.dosage = result.dosage;

            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }))
            }
        })    }
    toggleForm() {
        this.setState({selected: !this.state.selected});
    }


    reload() {
        this.setState({
            isLoaded: false
        });
        this.toggleForm();
        this.fetchMedications();
    }

    reload1() {
        this.setState({
            isLoaded: false
        });
        this.state.updateForm = false;

        this.fetchMedications();
    }

    handleId = (id) => {
        this.getMedication(id);
        this.state.id = id;
        this.state.updateForm = true;
        this.toggleForm();
    }

    render() {
        if (localStorage.getItem('user') === "doctor")
         return (
            <div>
                <CardHeader>
                    <span className={"patient-title"}><strong> Medication Management </strong></span>
                </CardHeader>
                <Card className={"table-background"}>
                    <div className={"breaker"}></div>
                    <Row className={"table-row"}>
                        <Col sm={{size: '8', offset: 1}}>
                            <Button className={"loginButton"} onClick={this.toggleForm}>Add Medication </Button>
                        </Col>
                    </Row>
                    <div className={"breaker"}></div>
                    <Row className={"table-row"}>
                        <Col sm={{size: '10', offset: 1}}>
                            {this.state.isLoaded && <MedicationTable tableData = {this.state.tableData} reloadHandler1={this.reload1} id={this.handleId}/>}
                            {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                errorStatus={this.state.errorStatus}
                                error={this.state.error}
                            />   }
                        </Col>
                    </Row>
                </Card>

                <Modal isOpen={this.state.selected} toggle={this.toggleForm}
                       className={this.props.className} size="lg">
                    {this.state.updateForm === false ?
                        <ModalHeader toggle={this.toggleForm}> Add Medication: </ModalHeader> :
                        <ModalHeader toggle={this.toggleForm}> Edit Medication: </ModalHeader> }

                    <ModalBody>
                        { this.state.updateForm === false ?
                            <MedicationForm reloadHandler={this.reload}  />:
                            <MedicationUpdateForm reloadHandler={this.reload} medication={this.medication} />

                        }

                    </ModalBody>
                </Modal>

            </div>
        )

        else return (<Redirect to={{
            pathname: '/login'

        }} />)

    }
}


export default MedicationContainer;
