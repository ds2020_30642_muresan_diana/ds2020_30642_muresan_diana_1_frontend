import React, {useState} from 'react'

import * as API_USERS_2 from "../account/api/account-api"
import {Link, Redirect} from "react-router-dom";
import {Label} from "reactstrap";
import BackgroundImg from "../commons/images/medical1.jpg";

const backgroundStyle = {
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    width: "100%",
    height: "900px",
    backgroundImage: `url(${BackgroundImg})`
};
class Login extends React.Component {


    constructor(props) {
        super(props);
        this.username = '';
        this.password = '';
        this.account = {
            id: null,
            username: '',
            password: '',
            role: ''
        };
        this.handleLogin = this.handleLogin.bind(this);
        this.state ={
            isLogged: false,
            username: '',
            role: ''
        }

    }

    handleLogin() {
        console.log(this.username);
        console.log(this.password);
        this.checkAccount(this.username);
    }

    checkAccount(username) {
        return API_USERS_2.getAccountByUsername(username, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201) && result.password === this.password) {
                console.log("Successfully checked account with id: " + result);
                this.account.id = result.id;
                this.account.username = result.username;
                this.account.password = result.password;
                this.account.role = result.role;
                this.setState(() => ({
                    isLogged: true,
                    username: result.username,
                    role: result.role
                }))
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error,
                    isLogged: false
                }));
            }
        });
    }

     render() {
         const { isLogged, role } = this.state;

         if (isLogged === true && role === 'caregiver')
         {
             localStorage.setItem('user', 'caregiver');
             console.log(isLogged);
             return (<Redirect to={{
                 pathname: '/caregiver',
                 state: {username: this.state.username}

             }} />)

         }

         if (isLogged === true && role === 'doctor')
         {

             localStorage.setItem('user', 'doctor');
             console.log(isLogged);
             return (<Redirect to={{
                 pathname: '/doctor'
             }}
             />)
         }

         if (isLogged === true && role === 'patient')
         {
             localStorage.setItem('user', 'patient');
             console.log(isLogged);
             return (<Redirect to={{
                 pathname: '/patient',
                 state: {username: this.state.username}
             }} />)
         }

         return (
             <div style={backgroundStyle}>

                    <div className="login">
                         <h1>Login</h1>
                        <div className={"loginInput"}>
                            <Label>Username:</Label>
                             <input
                             type="text"
                                 onChange={(e) => {
                                 this.username = (e.target.value);
                                }}

                             />
                        </div>
                        <div className={"loginInput"}>
                             <Label>Password:</Label>
                            <input
                            type={"password"}
                            onChange={(e) => {
                                 this.password = (e.target.value);
                            }}

                        />
                        </div>
                        <div className={"loginButton"}>
                            <button   onClick={this.handleLogin}> Login</button>
                        </div>
                    </div>

             </div>

         )
     }
}

export default Login;

