import React from 'react';
import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import {
    Button,
    Card,
    CardHeader,
    Col, FormGroup, Input, Label,
    Modal,
    ModalBody,
    ModalHeader,
    Row
} from 'reactstrap';

import * as API_USERS from "../caregiver/api/caregiver-api"
import CaregiverTable from "../caregiver/components/caregiver-table";
import PatientTable from "../patient/components/patient-table";
import * as API_USERS_2 from "../patient/api/patient-api"
import {Redirect} from "react-router-dom";






class Caregiver extends React.Component {

    constructor(props) {
        super(props);
        this.dataSent = props.location;
        //this.username = props.location.state.username;
       // console.log(props);
        //console.log(props.location.state.username);
        this.reload = this.reload.bind(this);
        this.handleLogout = this.handleLogout.bind(this);


        this.caregiver = {
            id: null,
            name: null,
            birthdate: null,
            gender: null,
            address: null,
            username: null,
            password: null
        }
        this.state = {
            tableData: [],
            isLoaded: false,
            errorStatus: 0,
            error: null,
            logout: false


        };
    }

    componentDidMount() {
        if (localStorage.getItem('user') === "caregiver") {
            this.fetchCaregiver(this.dataSent.state.username);
            this.fetchPatientsByCaregiver(this.dataSent.state.username);
        }

    }

    fetchPatientsByCaregiver(username) {
        return API_USERS_2.getPatientsByCaregiver(username,(result, status, err) => {

            if (result !== null && status === 200) {
                this.setState({
                    tableData: result,
                    isLoaded: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }


    reload() {
        this.setState({
            isLoaded: false
        });

        this.fetchPatients();
    }

    handleLogout() {
        this.setState({
            logout: true
        })
    }
    fetchCaregiver(username) {
        return API_USERS.getCaregiverByUsername(username,(result, status, err) => {

            if (result !== null && status === 200) {
                this.caregiver.id = result.id;
                this.caregiver.name = result.name;
                this.caregiver.address = result.address;
                this.caregiver.gender = result.gender;
                this.caregiver.birthdate = result.birthdate;
                this.caregiver.username = result.username;
                this.caregiver.password = result.password;
                console.log(this.caregiver);
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }



    render() {
        const { logout } = this.state;
        if ( logout === true ) {
            localStorage.removeItem('user');
            return (<Redirect to={{
                pathname: '/login'

            }} />)
        }
        if (localStorage.getItem('user') === "caregiver")
         return (
            <div>

                <CardHeader>
                    <span className={"info-title"}><strong> Your Patients </strong></span>
                    <button className={"logout-button"} onClick={this.handleLogout}> Logout</button>
                </CardHeader>

                <Card className={"table-background"}>

                    <Row className={"table-row"}>
                        <Col sm={{size: '10', offset: 1}} >
                            {this.state.isLoaded && <PatientTable tableData = {this.state.tableData} reloadHandler={this.reload} viewOnly={0} cols={0}/>}
                            {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                errorStatus={this.state.errorStatus}
                                error={this.state.error}
                            />   }
                        </Col>
                    </Row>

                </Card>
            </div>
        )

        else return (<Redirect to={{
            pathname: '/login'

        }} />)

    }
}


export default Caregiver;
