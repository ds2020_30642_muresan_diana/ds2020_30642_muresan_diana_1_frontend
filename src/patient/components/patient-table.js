import React from "react";
import Table from "../../commons/tables/table";
import {Button} from "react-bootstrap";
import * as API_USERS from "../api/patient-api";
import {Modal, ModalBody, ModalHeader} from "reactstrap";
import PatientForm from "./patient-form";


class PatientTable extends React.Component {

    constructor(props) {
        super(props);
        this.reloadHandler1 = this.props.reloadHandler1;
        this.viewOnly = this.props.viewOnly;
        this.cols = this.props.cols;
        this.state = {
            tableData: this.props.tableData,
            patientData: this.props.patientData
        };
    }
    columns2 = [
        {
            Header: 'Name',
            accessor: 'name',
        },
        {
            Header: 'Birthdate',
            accessor: 'birthdate',
        },
        {
            Header: 'Gender',
            accessor: 'gender',
        },
        {
            Header: 'Address',
            accessor: 'address',
        },
        {
            Header: 'Medical Record',
            accessor: 'medicalRecord',
        },

        {
            Header: 'Username',
            accessor: 'username',
        }
];
    columns1 = [

        {
            Header: 'Name',
            accessor: 'name',
        },
        {
            Header: 'Birthdate',
            accessor: 'birthdate',
        },
        {
            Header: 'Gender',
            accessor: 'gender',
        },
        {
            Header: 'Address',
            accessor: 'address',
        },
        {
            Header: 'Medical Record',
            accessor: 'medicalRecord',
        },

        {
            Header: 'Username',
            accessor: 'username',
        },
        {
            Header: 'Password',
            accessor: 'password',
        },



        {
            Header: ' ',
            accessor: 'id',
            Cell:(cell)=>(<button style={{backgroundColor: 'red', borderStyle: "none", color: 'white'}} onClick={()=>{ console.log(cell.value);
                this.handleDelete(cell.value);
            }}> Delete </button>)
        },
        {
            Header: ' ',
            accessor: 'id',

            Cell:(cell)=>(<button style={{backgroundColor: 'green', borderStyle: "none", color: 'white'}} onClick={()=>{

                this.handleId(cell.value, 1);
            }}> Update </button>)

        },
        {
            Header: '',
            accessor: 'id',

            Cell:(cell)=>(<button style={{backgroundColor: 'blue', borderStyle: "none", color: 'white'}} onClick={()=>{

                this.handleId(cell.value, 0);
            }}> Medication Plan </button>)

        }



    ];

    filters = [
        {
            accessor: 'name',
        }
    ];

    handleId(id, val) {
        let params = [id, val];
        this.props.params(params);
        return params;
    }


    handleDelete(id) {
        return API_USERS.deletePatient(id, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully deleted patient with id: " + result);
                this.reloadHandler1();
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });
    }

    render() {
        return (

            <Table
                data={this.viewOnly === 1 ? this.state.patientData : this.state.tableData}
                columns={this.cols === 0 ? this.columns2 : this.columns1}
                search={this.filters}
                pageSize={5}
                onChange={this.handleId}

            />





        )
    }
}

export default PatientTable;
