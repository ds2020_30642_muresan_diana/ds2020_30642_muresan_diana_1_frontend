import React from "react";
import Table from "../../commons/tables/table";
import {Button} from "react-bootstrap";
import * as API_USERS from "../api/patient-api";
import {Modal, ModalBody, ModalHeader} from "reactstrap";
import PatientForm from "./patient-form";


class MedicationTableMedplan extends React.Component {

    constructor(props) {
        super(props);


        this.state = {

            tableData: this.props.tableData
        };
    }
    columns = [
        {
            Header: 'Name',
            accessor: 'name',
        },
        {
            Header: 'Side Effects',
            accessor: 'sideEffects',
        },
        {
            Header: 'Dosage',
            accessor: 'dosage',
        },
        {
            Header: '',
            accessor: 'id',
            Cell:(cell)=>(<button style={{backgroundColor: 'red', borderStyle: "none", color: 'white'}} onClick={()=>{
                this.handleSelect(cell.value);
            }}> Select </button>)
        }
    ];


    filters = [
        {
            accessor: 'name',
        }
    ];


    handleSelect(medicationId) {
        this.props.medicationId(medicationId);


        return medicationId;
    }

    render() {
        return (
            <Table
                data={this.state.tableData}
                columns={  this.columns}
                search={this.filters}
                pageSize={5}
                onChange={this.handleSelect}


            />




        )
    }
}

export default MedicationTableMedplan;
