import React from 'react';
import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import {
    Button,
    Card,
    CardHeader,
    Col,
    Modal,
    ModalBody,
    ModalHeader,
    Row
} from 'reactstrap';

import * as API_USERS from "./api/patient-api"


import PatientTable from "./components/patient-table";
import PatientForm from "./components/patient-form";
import MedicationPlanForm from "./components/medication-plan-form";
import PatientUpdateForm from "./components/patient-update-form";
import moment from "moment";
import CaregiverForm from "../caregiver/components/caregiver-form";
import CaregiverUpdateForm from "../caregiver/components/caregiver-update-form";
import {Redirect} from "react-router-dom";


class PatientContainer extends React.Component {

    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);

        this.reload = this.reload.bind(this);
        this.reload1 = this.reload1.bind(this);

        this.patientId = null;
        this.patient =   {
            id: null,
            name: null,
            birthdate: null,
            gender: null,
            address: null,
            medicalRecord: null,
            username: null,
            password: null,
            medicationPlans: null
        };


        this.state = {
            medicationPlan: false,
            updateForm: false,
            selected: false,
            collapseForm: false,
            tableData: [],

            isLoaded: false,
            errorStatus: 0,
            error: null,
            params: []

        };
    }

    componentDidMount() {
        this.fetchPatients();

    }

    fetchPatients() {
        return API_USERS.getPatients((result, status, err) => {

            if (result !== null && status === 200) {
                this.setState({
                    tableData: result,
                    isLoaded: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }


    getPatient(id) {
        return API_USERS.getPatientById(id, (result, status, err) => {
            if (result !== null && status === 200) {
                let formatBirthdate = moment(result.birthdate).format('DD/MM/YYYY');
                this.patient.id = result.id;
                this.patient.name = result.name;
                this.patient.birthdate = formatBirthdate;
                this.patient.gender = result.gender;
                this.patient.address = result.address;
                this.patient.medicalRecord = result.medicalRecord;
                this.patient.username = result.username;
                this.patient.password = result.password;
                this.patient.medicationPlans = result.medicationPlans;
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }))
            }
        })
    }


    toggleForm() {
        this.setState({selected: !this.state.selected});

    }

    reload() {
        this.setState({
            isLoaded: false
        });

        this.toggleForm();


        this.fetchPatients();
    }

    reload1() {
        this.setState({
            isLoaded: false
        });
        this.state.updateForm = false;
        this.fetchPatients();
    }


    handleId  = (params) => {

        console.log(params);
        this.getPatient(params[0]);
        console.log(params[0]);
        this.patientId = params[0];
        this.state.params[0] = params[0];
        console.log(params[0]);

        if (params[1] === 1) {
            this.state.updateForm = true;
            this.state.medicationPlan = false;
        }
        else {
            this.state.medicationPlan = true;

        }
        this.toggleForm();

    }



    render() {
        if(localStorage.getItem('user') === "doctor")
            return (
            <div>
                <CardHeader>
                    <span class={"patient-title"}><strong> Patient Management </strong></span>
                </CardHeader>
                <Card className={"table-background"}>
                    <div className={"breaker"}></div>
                    <Row className={"table-row"}>
                        <Col sm={{size: '8', offset: 1}}>
                            <Button className={"loginButton"} onClick={this.toggleForm}>Add Patient </Button>
                        </Col>
                    </Row>
                    <div className={"breaker"}></div>


                    <Row className={"table-row"}>
                        <Col sm={{size: '12', offset: 0}}>

                            {this.state.isLoaded && <PatientTable tableData = {this.state.tableData} reloadHandler1={this.reload1} params={this.handleId} cols={1}/>}
                            {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                errorStatus={this.state.errorStatus}
                                error={this.state.error}
                            />   }
                        </Col>
                    </Row>
                </Card>

                <Modal isOpen={this.state.selected} toggle={this.toggleForm}
                       className={this.props.className} size="lg">
                    {this.state.updateForm === false && this.state.medicationPlan === false  && <ModalHeader toggle={this.toggleForm}> Add Patient: </ModalHeader>}
                    {this.state.updateForm === true && this.state.medicationPlan === false  && <ModalHeader toggle={this.toggleForm}> Edit Patient: </ModalHeader>}
                    {this.state.medicationPlan === true  && <ModalHeader toggle={this.toggleForm}> Create Medication Plan: </ModalHeader>}


                    <ModalBody>

                        {this.state.updateForm === false && this.state.medicationPlan === false && <PatientForm reloadHandler={this.reload}/>}
                        {this.state.updateForm === true && this.state.medicationPlan === false && <PatientUpdateForm reloadHandler={this.reload} patient={this.patient}/>}
                        {this.state.medicationPlan === true && <MedicationPlanForm patientId={this.patientId} reloadHandler={this.reload} />}
                        {this.state.updateForm  = false}
                        {this.state.medicationPlan = false}
                    </ModalBody>
                </Modal>



            </div>
        )

        else return (<Redirect to={{
            pathname: '/login'

        }} />)

    }
}


export default PatientContainer;
