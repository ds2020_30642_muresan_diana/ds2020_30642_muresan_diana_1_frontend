import {HOST} from "../../commons/hosts";
import RestApiClient from "../../commons/api/rest-client";

const endpoint = {
    account: '/account'
};

function getAccounts(callback) {
    let request = new Request(HOST.backend_api + endpoint.account, {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function postAccount(user, callback) {
    let request = new Request(HOST.backend_api + endpoint.account, {
        method: 'POST',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    });
    console.log("URL: " + request.url);
    RestApiClient.performRequest(request, callback);
}

function getAccountByUsername(params, callback) {
    let request = new Request(HOST.backend_api + endpoint.account + '/get-by-username/' + params, {
        method: 'GET'
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}
function updateAccount(user, callback) {
    let request = new Request(HOST.backend_api + endpoint.account, {
        method: 'PUT',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    });
    console.log("URL: " + request.url);
    RestApiClient.performRequest(request, callback);
}


export {
    getAccounts,
    postAccount,
    getAccountByUsername,
    updateAccount
};